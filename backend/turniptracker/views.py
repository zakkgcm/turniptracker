from django.shortcuts import render
from django.utils import timezone
from django.http import HttpResponseRedirect, HttpResponseServerError
from django.urls import reverse

from .models import TurnipRecord
from .forms import TurnipRecordForm


def index(request):
    turnip_records = TurnipRecord.objects.order_by('-timestamp')

    # form processing
    if request.method == 'POST':
        form = TurnipRecordForm(request.POST)

        if form.is_valid():
            record = TurnipRecord(
                resident=request.POST['resident'],
                price=request.POST['price'],
                timezone=request.POST['timezone'])

            record.save()
            return HttpResponseRedirect(reverse('turniptracker:index'))
    else:
        form = TurnipRecordForm()

    ctx = {'turnip_records': turnip_records, 'form': TurnipRecordForm()}

    return render(request, 'turniptracker/index.html', context=ctx)
