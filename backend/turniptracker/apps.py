from django.apps import AppConfig


class TurniptrackerConfig(AppConfig):
    name = 'turniptracker'
