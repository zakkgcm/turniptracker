from django.db import models
from datetime import datetime, timedelta

import pytz
from pytz import timezone
from pytz.exceptions import UnknownTimeZoneError


class TurnipRecord(models.Model):
    resident = models.CharField(max_length=200)
    price = models.IntegerField()
    timestamp = models.DateTimeField(auto_now_add=True)
    timezone = models.CharField(max_length=255, default='UTC')
    
    @property
    def pytz_timezone(self):
        try:
            return timezone(self.timezone)
        except UnknownTimeZoneError:
            pass

        return pytz.utc

    @property
    def local_timestamp(self):
        return self.timestamp.astimezone(self.pytz_timezone)

    @property
    def local_time(self):
        return datetime.now().astimezone(self.pytz_timezone)

    @property
    def valid(self):
        tz = self.pytz_timezone
        localized_ts = self.timestamp.astimezone(tz)
        localtime = datetime.now().astimezone(tz)

        # should we just check that the dates match?
        if localized_ts.date() < localtime.date():
            return False

        # if localized_ts < noon, then this was a morning price
        if localized_ts.hour < 12:
            return localtime.hour < 12
        # ottherwise, check if nook's is closed (after 10PM)
        else:
            return localtime.hour < 22
